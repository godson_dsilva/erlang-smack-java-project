var config = {
	newsurls : [
				{
					source: 'CNN',
					category: 'Golf',
					url: 'http://rss.cnn.com/rss/edition_golf.rss'
				 },
				{
					source: 'CNN',
					category: 'Motorsport',
					url:'http://rss.cnn.com/rss/edition_motorsport.rss'
				},
				{
					source: 'CNN',
					category: 'Tennis',
					url:'http://rss.cnn.com/rss/edition_tennis.rss'
				},
				{
					source: 'CNN',
					category: 'Football',
					url:'http://rss.cnn.com/rss/edition_football.rss'
				},
				{
					source: 'CNN',
					category: 'General',
					url:'http://rss.cnn.com/rss/edition_sport.rss'
				}

	],
	redisPort: 6379,
	redisServer: '54.169.211.211',
	token: 'C15B66667F1E7D25DDA2A64424693',
	scheduleMinute: 2,
	redisKey: 'sporto-news'
}

module.exports = config;
