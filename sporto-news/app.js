var express   = require('express');
var app       = express();
var http      = require('http');
var redis     = require('redis');
var feed      = require('feed-read');
var gfeed     = require('google-feed-api');
var schedule  = require('node-schedule');
var morgan    = require('morgan');
var bodyParser = require('body-parser');
var config    = require('./config');
var client    = redis.createClient(config.redisPort, config.redisServer);
var token     = config.token;
var urls      = config.newsurls;


// ///////////////////////////////////////////////////
// All environments
// ///////////////////////////////////////////////////
app.set('env', 'development');
app.set('port', process.env.PORT || 3001);
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

// ///////////////////////////////////////////////////
// CORS
// ///////////////////////////////////////////////////
app.all('/*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
    if (req.method == 'OPTIONS') {
      res.status(200).end();
    } else {
      next();
    }
  });

function News(title, linkUrl, publishDate, imageUrl,contentSnippet,source,category) {
 this.Title = title;
 this.LinkUrl = linkUrl;
 this.PublishDate = publishDate;
 this.ImageUrl = imageUrl;
 this.ContentSnippet = contentSnippet;
 this.Source = source;
 this.Category = category;
}

function ListItems(feed, source, category) {
    feed.listItems(function(items){
        items.forEach(function(item){
          var title = item.title;
          var link = item.link;
          var  publishedDate = item.publishedDate;
          mediaGroups = item.mediaGroups;
          var ImageURl=  mediaGroups[0].contents[0]['url'];
          var contentSnippet = item.contentSnippet;
          var newsObject = new News(title,link,publishedDate,ImageURl,contentSnippet,source,category);

          arrayList = arrayList.concat(newsObject);
          client.set(config.redisKey, JSON.stringify(arrayList));
      });
   });
}

var arrayList= [];  
var rule = new schedule.RecurrenceRule();
// rule.minute = config.scheduleMinute;
rule.hour = 22;  
 schedule.scheduleJob(rule, function(){
    console.log("Function Called");
    arrayList= [];
    var mediaGroups= [];
    var contents= [];

    for (var j = 0; j < urls.length; j++) {
      var feed = new gfeed.Feed(urls[j].url);
      var source = urls[j].source;
      var category = urls[j].category;
      var listObject = new ListItems(feed,source, category);
    }
})

app.get('/redis_connect', function (req, res) {
  client.on('connect', function() {
    console.log('Redis server connected !');
  });
})

app.get('/api/news', function(req, res){
 var accessToken = req.headers['x-access-token']; 
 if(accessToken == config.token)
 {
  client.get(config.redisKey, function (err, replies) {
    var json = JSON.parse(replies);
    res.status(200).send(json);
  });
}
else{
  res.status(401).send({error: true, description: 'Invalid access token' });
}         
});


// ///////////////////////////////////////////////////
// Server
// ///////////////////////////////////////////////////
http.createServer(app).listen(app.get('port'), function(){
  console.log('Sporto news express server listening at: http://localhost:%d/', app.get('port'));
});
